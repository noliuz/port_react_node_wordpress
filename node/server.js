const express = require('express')
const fs      = require('fs');
const app = express()
const nocache = require('nocache')
app.use(nocache())
const cors = require('cors')
app.use(cors())
const port = 4000
const dbcon = require('./mysql').con;

app.get('/get_posts_count',async (req,res) => {
  let q = `SELECT count(ID) as count
           FROM wp_posts
           WHERE post_status LIKE "publish"
           AND post_type LIKE "post"
          `
  let dbres = await dbcon.query(q);
  dbres = dbres[0][0].count
  res.send(dbres+'')
})

app.get('/get_post_from_id/:id',async (req,res) => {
  let q = `SELECT ID,post_content,post_title,wt.name as cat_name FROM wp_posts p
           INNER JOIN wp_term_relationships r ON r.object_id=p.ID
           INNER JOIN wp_term_taxonomy t ON t.term_taxonomy_id = r.term_taxonomy_id
			     INNER JOIN wp_terms wt on wt.term_id = t.term_id
           WHERE post_status LIKE "publish"
           AND post_type LIKE "post"
           AND ID = ${req.params.id}
           ORDER BY ID DESC
          `
  let dbres = await dbcon.query(q);
  //console.log(dbres[0])
  dbres = dbres[0]
  //get image path
  let count = 0;
  for (let item of dbres) {
    let q = `SELECT guid FROM wp_posts
             WHERE post_mime_type LIKE "image%" AND
             post_parent = ${item.ID}
            `
    let dbres2 = await dbcon.query(q)
    let guid = dbres2[0][0].guid

    dbres[count].file_path = guid
    count++
  }
  res.send(dbres)
})

app.get('/get_posts_from_cat/:cat', async (req,res) => {
  let q = `SELECT ID,post_content,post_title,wt.name as cat_name FROM wp_posts p
           INNER JOIN wp_term_relationships r ON r.object_id=p.ID
           INNER JOIN wp_term_taxonomy t ON t.term_taxonomy_id = r.term_taxonomy_id
			     INNER JOIN wp_terms wt on wt.term_id = t.term_id
           WHERE post_status LIKE "publish"
           AND post_type LIKE "post"
           AND wt.name LIKE "${req.params.cat}"
           ORDER BY ID DESC
          `
  let dbres = await dbcon.query(q);
  dbres = dbres[0]
  let count = 0;
  for (let item of dbres) {
    let q = `SELECT guid FROM wp_posts
             WHERE post_mime_type LIKE "image%" AND
             post_parent = ${item.ID}
            `
    let dbres2 = await dbcon.query(q)
    let guid = dbres2[0][0].guid

    dbres[count].file_path = guid
    count++
  }
  //limit content length
  for (let item of dbres) {
    item.post_content = item.post_content.substring(0,200)
  }

  res.send(dbres)
})

app.get('/get_posts/:limit', async (req, res) => {
  //get posts
  let q = `SELECT ID,post_content,post_title,wt.name as cat_name FROM wp_posts p
           INNER JOIN wp_term_relationships r ON r.object_id=p.ID
           INNER JOIN wp_term_taxonomy t ON t.term_taxonomy_id = r.term_taxonomy_id
			     INNER JOIN wp_terms wt on wt.term_id = t.term_id
           WHERE post_status LIKE "publish"
           AND post_type LIKE "post"
           ORDER BY ID DESC
           LIMIT ${req.params.limit}
          `
  let dbres = await dbcon.query(q);
  dbres = dbres[0]
  //res.send(dbres)
  //get image
  let count = 0;
  for (let item of dbres) {
    let q = `SELECT guid FROM wp_posts
             WHERE post_mime_type LIKE "image%" AND
             post_parent = ${item.ID}
            `
    let dbres2 = await dbcon.query(q)
    let guid = dbres2[0][0].guid
    dbres[count].file_path = guid
    count++
  }
  //limit content length
  for (let item of dbres) {
    item.post_content = item.post_content.substring(0,200)
  }

  res.send(dbres)
})

app.get('/get_latest_post_id',async (req,res) => {
  let q = `SELECT ID FROM wp_posts p
           WHERE post_status LIKE "publish"
           AND post_type LIKE "post"
           ORDER BY ID DESC
          `
  let dbres = await dbcon.query(q)
  res.send(dbres[0][0].ID+'')
})

app.get('/get_category', async (req, res) => {
  let q = `select distinct term_taxonomy_id cat_id from wp_term_relationships
           order by cat_id DESC
          `
  let dbres = await dbcon.query(q)
  dbres = dbres[0]
  let catArr = []
  for (let i=0;i<dbres.length;i++) {
    q = ` SELECT name FROM wp_terms WHERE term_id = ${dbres[i].cat_id}
      `
    let dbres2 = await dbcon.query(q)
    dbres2 = dbres2[0][0].name
    catArr.push(dbres2)
  }
  res.send(catArr)
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
