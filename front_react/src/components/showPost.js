import React from 'react';
import {useParams} from 'react-router'
import Header from './header'
import HomeSinglePost from './homeSinglePost'

const ShowPost = () => {
  const {id} = useParams()
  return (
    <div>
      <Header />
      <HomeSinglePost id={id}/>
    </div>
  )
}

export default ShowPost
