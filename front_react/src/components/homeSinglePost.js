import React,{useState,useEffect} from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
const config = require('../config')

//import post1Img from '../assets/img/post1.png'


const HomeSinglePost = ({id}) => {
  const [showData,setShowData] = useState([])

  const getPostFromID = async () => {
    let res = await fetch(config.backend_url+'get_post_from_id/'+id)
    let data = await res.json()
    if (data.length != 0)
      setShowData(data[0])
  }

  useEffect(()=>{
    getPostFromID()
    //console.log(id)

  },[id])
  return (
    <Container>
      <Box sx={{textAlign:'left',mt:5}}>
        <Typography sx={{fontSize:20,color:'#b4ad9e'}}>
            {showData.cat_name}
        </Typography>
        <Typography sx={{color:'#626262',fontSize:40,mt:2}}>
          {showData.post_title}
        </Typography>
        <img src={showData.file_path} style={{width:"50%",margin:'auto',display:'block',marginTop:'20px',marginBottom:'20px'}} />
        <div
            style={{color:'#626262',fontSize:15,marginTop:4  }}
            dangerouslySetInnerHTML={{__html:showData.post_content}}
          >
        </div>

      </Box>
    </Container>
  )

}

export default HomeSinglePost
