import React,{useEffect,useState} from 'react';
import Home2ColumnPost from './home2ColumnPost'
import Header from './header'
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
var config = require('../config')

var postCount = 2
var maxPostCount = 2
const Blog = () => {
  const [data2,setData2] = useState([])
  const [catList,setCatList] = useState([])

  const getPostFromCat = async(cat) => {
    let res = await fetch(config.backend_url+'get_posts_from_cat/'+cat)
    let data = await res.json()
    let dataCouple = changePostData2Couple(data)
    maxPostCount = 0
    setData2(dataCouple)

  }
  const getCategory = async () => {
    let res = await fetch(config.backend_url+'get_category')
    let data = await res.json()
    setCatList(data)
    //console.log(data)
  }
  const onLoadMoreClick = async () => {
    postCount += 2
    await getRequest()

  }
  const getRequestPostCount = async () => {
    let res = await fetch(config.backend_url+'get_posts_count')
    let data = await res.json()
    maxPostCount = data
  }
  const changePostData2Couple = (data) => {
    let dataTwo = []
    //add null data
    if (data.length%2 == 1) {
      data.push({ID:null,post_content:null,post_title:null,cat_name:null,file_path:null})
    }
    //map 2 couple data
    for (let i=0;i<data.length;i++) {
      if (i%2 === 0) {
        let coupleData = {}
        coupleData.one = data[i]
        coupleData.two = data[i+1]
        dataTwo.push(coupleData)
      }
    }
    return dataTwo
  }
  const getRequest = async () => {
    //console.log(postCount)
    let res = await fetch(config.backend_url+'get_posts/'+postCount)
    let data = await res.json()
    let dataCouple = changePostData2Couple(data)
    setData2(dataCouple)
    //console.log(data2)
  }
  const onCatClick = async (cat) => {
    getPostFromCat(cat)
  }

  useEffect(async ()=> {
    await getRequestPostCount()
    await getRequest()
    //await getPostFromCat()
    await getCategory()
  },[])

  return (
    <Container maxWidth='xl'>
      <Header />
      <Grid container>
        <Grid item sx={{width:'200px'}}>

          <Typography sx={{color:'#5c5c5c',fontSize:'25px',textAlign:'left',ml:3,mt:8}}>
            Category
          </Typography>
          { catList.map((item)=> {
              return (
                <Typography
                  sx={{textAlign:'left',ml:3,cursor:'pointer',mt:3}}
                  onClick={()=>{onCatClick(item)}}
                >
                  {item}
                </Typography>
              )
            })

          }
        </Grid>
        <Grid item>
         {
            data2.map((coupleItem)=> {
              //console.log(coupleItem)

              return (
                <Home2ColumnPost
                  ID1={coupleItem.one.ID}
                  img1={coupleItem.one.file_path}
                  content1={coupleItem.one.post_content}
                  cat1={coupleItem.one.cat_name}
                  title1={coupleItem.one.post_title}
                  ID2={coupleItem.two.ID}
                  img2={coupleItem.two.file_path}
                  content2={coupleItem.two.post_content}
                  cat2={coupleItem.two.cat_name}
                  title2={coupleItem.two.post_title}
                />
              )
            })


          }
          { (postCount < maxPostCount)?
              <Button
                variant='outlined'
                sx={{color:'#5d5d5d',borderColor:'#5d5d5d',borderRadius:0,mt:10,maxWidth:'300px',mb:5}}
                onClick={onLoadMoreClick}
              >
                Load More
              </Button>
              :''
          }
        </Grid>

      </Grid>
    </Container>
  )
}

export default Blog
