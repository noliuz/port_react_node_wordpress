import avatarImg from '../assets/img/avatar.jpg'
import Container from '@mui/material/Container';
import Header from './header'

const About = () => {
  return (
    <Container maxWidth="xl">
      <Header />
      <div>
        <img src={avatarImg} style={{width:376,borderRadius:'50%'}}/>
      </div>
      <div><h2 style={{color:'#5e5e5e'}}>About Me</h2></div>
      <p style={{color:'#5d5d5d',textAlign:'left'}}>
      My name is Nol Limprasert. I am an expert developer in IT fields. Several experiences show my skill that
can code many tools and aspects. 16+ years in these fields can guarantee my
ability. Honesty and responsibility are my core natures. Employer pays me to work
for them so I must work
for them. My life style is traveling in natural and quiet places.</p>
    </Container>
  )
}

export default About
