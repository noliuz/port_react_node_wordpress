import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';

const BigEmail = () => {
  return (
    <Container maxWidth='xl'>
      <Box sx={{backgroundColor:'#f3f3f3'}} mt={10} p={5}>
        <Typography
          sx={{color:'#2c2c2c',fontSize:40,mt:2  }}
        >My email address!
        </Typography>
        <Typography
          sx={{color:'#5b5b5b',fontSize:30,mt:2,textDecoration:'underline'}}
        >noliuz@gmail.com
        </Typography>

      </Box>
    </Container>
  )
}

export default BigEmail
