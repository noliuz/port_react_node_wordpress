import React,{useEffect,useState} from 'react';
import Header from './header'
import HomeSinglePost from './homeSinglePost'
import Banner from './banner'
import BigEmail from './bigEmail'
const config = require('../config')

const HomePage = () => {
  const [lastID,setLastID] = useState(0)

  useEffect(async ()=> {
    let res = await fetch(config.backend_url+'get_latest_post_id')
                    .catch((err) => {
                      console.log(err)
                    })
    let data = await res.json()
    setLastID(data)
    //console.log(data)
  },[lastID])
  return (
    <div>
      <Header />
      <Banner />
      <HomeSinglePost id={lastID}/>
      <BigEmail />
      <div style={{marginTop:10}}>&nbsp;</div>
    </div>
  )
}

export default HomePage
