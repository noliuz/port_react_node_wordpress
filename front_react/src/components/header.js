import React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/Menu';
import Container from '@mui/material/Container';
import Button from '@mui/material/Button';
import MenuItem from '@mui/material/MenuItem';
import logoImg from '../assets/img/logo.jpg'
import { useHistory } from "react-router-dom";

const pages = ['Blog', 'About'];

const Header = () => {
  const [anchorElNav, setAnchorElNav] = React.useState(null);
  const [anchorElUser, setAnchorElUser] = React.useState(null);

  const history = useHistory()
  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget)
  };
  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget)
  };

  const handleCloseNavMenu = (page) => {
    setAnchorElNav(null)
    page = page.toLowerCase()
    history.push('/'+page)
  };

  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  return (
    <Container maxWidth="xl" >

      <AppBar
        position="static"
        sx={{backgroundColor:'white',color:'black'}}
        elevation={0}
      >
        <Toolbar disableGutters >
          <img src={logoImg} style={{cursor:'pointer'}} onClick={()=>handleCloseNavMenu('')}/>

          <Box sx={{ justifyContent:'flex-end',
                     flexGrow: 1,
                     display: { xs: 'flex', md: 'none' }
                   }}>
            <IconButton
              size="large"
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={handleOpenNavMenu}
              color="inherit"
            >
              <MenuIcon />
            </IconButton>
            <Menu
              id="menu-appbar"
              anchorEl={anchorElNav}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left',
              }}
              keepMounted
              transformOrigin={{
                vertical: 'top',
                horizontal: 'left',
              }}
              open={Boolean(anchorElNav)}
              onClose={handleCloseNavMenu}
              sx={{
                display: { xs: 'block', md: 'none' },

              }}
            >
              {pages.map((page) => (
                <MenuItem key={page} onClick={()=>{handleCloseNavMenu(page)}}
                  sx={{
                    ':hover': {
                      backgroundColor:'#fff'
                    }

                  }}
                >
                  <Typography textAlign="center">{page}</Typography>
                </MenuItem>
              ))}
            </Menu>
          </Box>

          <Box sx={{
                    flexGrow: 1,
                    display: { xs: 'none', md: 'flex' },
                    justifyContent:'flex-end'
                  }}>
            {pages.map((page) => (
              <Button
                key={page}
                onClick={()=>{handleCloseNavMenu(page)}}
                sx={{
                    my: 2,
                    color: 'black',
                    display: 'block',
                    fontSize:20,
                    mr:15,
                    ':hover': {
                      backgroundColor:'#fff'
                    }
               }}
              >
                {page}
              </Button>
            ))}
          </Box>
        </Toolbar>

      </AppBar>


    </Container>

  );
};
export default Header;
