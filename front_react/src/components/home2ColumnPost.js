import React from 'react';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import { useHistory } from 'react-router-dom';

const Home2ColumnPost = ({ID1,img1,content1,cat1,title1,ID2,img2,content2,cat2,title2}) => {
  let history = useHistory();
  const onOneClick = () => {
    history.push('/showPost/'+ID1)
  }
  const onTwoClick = () => {
    history.push('/showPost/'+ID2)
  }
  return (
    <Container sx={{mt:10}}>
      <Grid container spacing={2}>
        <Grid item md={6} xs={12} sx={{display:'block'}}>
          <img src={img1} style={{width:'100%',cursor:'pointer'}} onClick={onOneClick}/>
          <Box sx={{textAlign:'left'}}>
            <Typography sx={{fontSize:20,color:'#b4ad9e',mt:2}}>{cat1}</Typography>
            <Typography sx={{color:'#626262',fontSize:40,mt:2,cursor:'pointer'}}
              onClick={onOneClick}
            >{title1}</Typography>
            <div
              style={{color:'#626262',fontSize:15,marginTop:2  }}
              dangerouslySetInnerHTML={{__html:content1}}
            >
            </div>
          </Box>
        </Grid>
        <Grid item md={6} xs={12} sx={{display:'block'}}>
          <img src={img2} style={{width:'100%',cursor:'pointer'}} onClick={onTwoClick}/>
          <Box sx={{textAlign:'left'}}>
            <Typography sx={{fontSize:20,color:'#b4ad9e',mt:2}}>{cat2}</Typography>
            <Typography sx={{color:'#626262',fontSize:40,mt:2,cursor:'pointer'}}
              onClick={onTwoClick}
            >{title2}</Typography>
            <div
              style={{color:'#626262',fontSize:15,marginTop:2  }}
              dangerouslySetInnerHTML={{__html:content2}}
            >
            </div>
          </Box>
        </Grid>


      </Grid>

    </Container>
  )

}

export default Home2ColumnPost
