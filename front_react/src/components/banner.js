import React from 'react';
import bannerImg from '../assets/img/banner.jpg'
import Container from '@mui/material/Container';

const Banner = () => {
  return (
    <Container maxWidth="xl">
      <img src={bannerImg} style={{width:"100%"}}/>
    </Container>
  )
}

export default Banner
