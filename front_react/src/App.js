import './App.css';
import Blog from './components/blog'
import HomePage from './components/homepage'
import ShowPost from './components/showPost'
import About from './components/about'
import { BrowserRouter as Router, Route, Link,Switch } from 'react-router-dom'
const config = require('./config')

function App() {
  return (
    <Router basename={config.route_basename}>
      <div className="App">
        <Switch>
          <Route path="/about"> <About /> </Route>
          <Route path="/showPost/:id"> <ShowPost /> </Route>
          <Route path="/blog"> <Blog /> </Route>
          <Route path="/"> <HomePage /> </Route>
        </Switch>
      </div>
    </Router>
  )
}

export default App;
